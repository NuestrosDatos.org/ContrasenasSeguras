# Contraseñas Seguras

Repositorio sobre Contraseñas Seguras que forma parte de los Talleres de Autodefensa Digital de [NuestrosDatos.org](https://nuestrosdatos.org)

El contenido del taller se encuentra en el archivo ***ContraseñasSeguras.org***

## Licencia

La licencia de este material es [Creative Commons Attribution-NonCommercial 4.0 International](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.es).

## Eventos

1. [Evento](https://nuestrosdatos.org/evento-taller-de-autodefensa-digital-contrasenas-seguras-1/) en colaboración con [Casa-Taller Esperanza](https://instagram.com/casa_talleresperanza).